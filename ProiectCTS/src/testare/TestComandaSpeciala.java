package testare;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.util.Date;

import org.junit.BeforeClass;
import org.junit.Test;

import clase.CartePrototypeFactory;
import clase.Comanda;
import clase.ComandaSpeciala;
import clase_abstracte.CarteAbstracta;
import clase_abstracte.ComandaAbstracta;

public class TestComandaSpeciala {
	private static ComandaAbstracta comanda;
	private static ComandaAbstracta comandaSpeciala;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		comanda = new Comanda();
		comanda.setDataComanda(new Date());
		CarteAbstracta carteAbstracta1 = CartePrototypeFactory.getPrototip("978-606-8623-10-8");
		CarteAbstracta carteAbstracta2 = CartePrototypeFactory.getPrototip("978-606-594-130-4");
		comanda.adaugaElementComanda(carteAbstracta1, 1);
		comanda.adaugaElementComanda(carteAbstracta2, 14);

		comandaSpeciala = new ComandaSpeciala(comanda);
	}

	@Test
	public void testValoareTotalaComandaSpeciala() {

		try {
			float valTotalaComanda = comanda.valoareTotala();
			float valTotalaComandaSpeciala = comandaSpeciala.valoareTotala();
			assertEquals("Verificare functie valoare totala pt decorator comanda speciala", valTotalaComanda + 100,
					valTotalaComandaSpeciala, 0.01);
		} catch (Exception e) {
			
		}
	}

	@Test
	public void testInstanteComandaSpeciala() {

		assertSame("Verificare functie valoare totala pt decorator comanda speciala", comandaSpeciala.getListaCarti(),
				comanda.getListaCarti());

	}
}
