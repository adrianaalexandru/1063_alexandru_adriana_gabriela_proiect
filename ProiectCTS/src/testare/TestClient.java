package testare;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import clase.Client;

public class TestClient {

	public Client client;

	public static final String numeInitial = "Adriana";
	public static final String nrTelefonInitial = "07376373673";
	public static final String emailInitial = "adrandd@yahoo.com";

	@Before
	public void setUp() throws Exception {

		this.client = new Client(numeInitial, nrTelefonInitial, emailInitial, null);
	}

	@After
	public void tearDown() throws Exception {
		this.client = null;
	}

	public void testSetNumeNull() {
		String nume = null;
		try {
			client.setNume(nume);
			fail("Numele accepta null!");
		} catch (Exception e) {

		}
	}

	@Test
	public void testSetNumeValoriNormale() {
		String nume = "Gabriela";
		client.setNume(nume);
		assertEquals(nume, client.getNume());

		nume = "Elena";
		client.setNume(nume);
		assertEquals(nume, client.getNume());
	}

	@Test
	public void testValoareNumeScurta() {
		String nume = "Abc";
		try {
			client.setNume(nume);
			fail("Numele accepta valori cu mai putin de 4 caractere!");
		} catch (Exception e) {
			System.out.println("S-a generat exceptie pt incercarea de a seta un nume cu mai putin de 4 caractere");
		}
	}

	@Test
	public void testSetNrTelefonValoriNormale() {
		String nrTelefon = "02292769297";
		client.setNrTelefon(nrTelefon);
		assertEquals(nrTelefon, client.getNrTelefon());

		nrTelefon = "01829265567";
		client.setNrTelefon(nrTelefon);
		assertEquals(nrTelefon, client.getNrTelefon());
	}

	@Test
	public void testSetNrTelefonNull() {
		String nrTelefon = null;
		try {
			client.setNrTelefon(nrTelefon);
			fail("Nr telefon accepta null!");
		} catch (Exception e) {

		}
	}

	@Test
	public void testNrTelefonScurt() {
		String nrTelefon = "07";
		try {
			client.setNrTelefon(nrTelefon);
			fail("Nr telefon accepta valori cu mai putin de 10 caractere!");
		} catch (Exception e) {
			System.out.println(
					"S-a generat eroare pentru incercarea de a seta un nr de telefon cu mai putin de 10 caractere");
		}
	}

	@Test
	public void testNrTelefonCuLitere() {
		String nrTelefon = "07accd76222";
		try {
			client.setNrTelefon(nrTelefon);
			fail("Nr telefon accepta valori cu litere!");
		} catch (Exception e) {
			System.out.println("S-a generat eroare pentru incercarea de a seta un nr de telefon cu litere");
		}
	}

	@Test
	public void testNumeCuCifre() {
		String nume = "Ioana12";
		try {
			client.setNume(nume);
			fail("Numele accepta valori cu cifre!");
		} catch (Exception e) {
			System.out.println("S-a generat eroare pentru incercarea de a seta un nume cu cifre");
		}
	}

	public void testSetEmailNull() {
		String email = null;
		try {
			client.setEmail(email);
			fail("Emailul accepta null!");
		} catch (Exception e) {
			System.out.println("S-a generat eroare pentru incercarea de a seta un email null");
		}
	}

	public void testSetEmailValoriNormale() {
		String email = "sjkans@yahoo.com";
		try {
			client.setEmail(email);
			assertEquals(email, client.getEmail());
		} catch (Exception e) {
			fail("Exceptie la setarea unei valori normale a emailului.");
		}
	}

	public void testSetEmailValoriGresite() {
		String email = "sjkansyahoo.com";
		try {
			client.setEmail(email);
			fail("Emailul accepta valori ce nu contin caracterul @");
		} catch (Exception e) {
			System.out.println("S-a generat exceptie la setarea unui email ce nu contine caracterul @");
		}
	}
}
