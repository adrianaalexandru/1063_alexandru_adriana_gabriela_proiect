package testare;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.mockito.Mockito.*;

import clase.Carte;
import clase.CartePrototypeFactory;
import clase.Comanda;
import clase.EBook;
import clase_abstracte.CarteAbstracta;

public class TestComanda {

	private static ArrayList<CarteAbstracta> cartiDinFisierText = new ArrayList<>();
	private static Scanner scan;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		try {
			scan = new Scanner(new File("carti.txt"));
			scan.useDelimiter("\t");
			int nrCarti = scan.nextInt();
			int tipCarte;
			for (int i = 0; i < nrCarti; i++) {
				tipCarte = scan.nextInt();
				if (tipCarte == 1) {
					Carte carteNoua = new Carte();
					carteNoua.citireFisierTextCarte(scan);
					cartiDinFisierText.add(carteNoua);
					System.out.println("carte noua" + carteNoua.getGen());
				} else {
					EBook ebookNou = new EBook();
					ebookNou.citireFisierTextEBook(scan);
					cartiDinFisierText.add(ebookNou);
					System.out.println("ebookNou nou" + ebookNou.getGen());
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		scan.close();
	}

	@Test
	public void testValoareTotala() {
		Comanda comanda = new Comanda();
		Carte carte1 =mock(Carte.class);
		Carte carte2 =mock(Carte.class);
		Carte carte3 =mock(Carte.class);
		when(carte1.getPret()).thenReturn(22f);
		when(carte2.getPret()).thenReturn(21f);
		when(carte3.getPret()).thenReturn(30f);
		try {
			comanda.adaugaElementComanda(carte1, 1);
			comanda.adaugaElementComanda(carte2, 2);
			comanda.adaugaElementComanda(carte3, 3);
		} catch (Exception e) {
			e.printStackTrace();
		}

		float valTotala;
		try {
			valTotala = comanda.valoareTotala();
			assertEquals("Verificare valoare comanda ",154, valTotala, 0.01);
		} catch (Exception e) {

		}
	}

	@Test
	public void testValoareTotalaListaNula() {
		Comanda comanda = new Comanda();
		comanda.setListaCarti(null);
		try {
			float valoareTot = comanda.valoareTotala();
			fail("Nu s-a generat exceptie pt lista nula");
		} catch (Exception exception) {
			System.out.println("S-a generat exceptie lista nula ");
		}
	}

	@Test
	public void testAdaugaElementComandaCantitateNegativa() {
		Comanda comanda = new Comanda();
		CarteAbstracta carteAbstracta1 = CartePrototypeFactory.getPrototip("978-606-8623-10-8");

		try {
			comanda.adaugaElementComanda(cartiDinFisierText.get(0), -1);
			fail("Exceptie nearuncata pentru adauga element avand cantitatea negativa");
		} catch (Exception e) {
			System.out.println("Exceptie aruncata pentru cantitate negativa");
		}
	}

	@Test
	public void testAdaugaElement() {
		Comanda comanda = new Comanda();

		CarteAbstracta carteAbstracta1 = CartePrototypeFactory.getPrototip("978-973-88-7495-4");
		comanda.adaugaElementComanda(carteAbstracta1, 10);
		float valInitiala;
		try {
			valInitiala = comanda.valoareTotala();
			CarteAbstracta carteAbstracta2 = CartePrototypeFactory.getPrototip("978-606-8623-10-8");

			try {
				comanda.adaugaElementComanda(carteAbstracta2, 10);
				assertEquals(valInitiala + carteAbstracta2.getPret() * 10, comanda.valoareTotala(), 0.1);
			} catch (Exception e) {
				System.out.println("Exceptie aruncata pentru adauga element cu valori corecte");
			}
		} catch (Exception e1) {
		}
	}

	@Test
	public void testRezultatGetNrProduseComandate() {
		Comanda comanda = new Comanda();
		CarteAbstracta carteAbstracta1 = CartePrototypeFactory.getPrototip("978-606-8623-10-8");
		CarteAbstracta carteAbstracta2 = CartePrototypeFactory.getPrototip("978-606-594-130-4");
		comanda.adaugaElementComanda(carteAbstracta1, 2);
		comanda.adaugaElementComanda(carteAbstracta2, 3);
		assertEquals(5, comanda.getNrProduseComandate());
	}

	@Test
	public void testGetPretMediuProdus() {
		Comanda comanda = new Comanda();
		assertEquals("Verificare get pret mediu produs pentru lista carti goala", 0, comanda.getPretMediuProdus(),
				0.01);
		comanda.adaugaElementComanda(cartiDinFisierText.get(0), 2);
		comanda.adaugaElementComanda(cartiDinFisierText.get(1), 3);
		assertEquals("Verificare get pret mediu produs pentru lista carti populata", 16, comanda.getPretMediuProdus(),
				0.1);

	}

	@Test
	public void testSetDiscount() {
		Comanda comanda = new Comanda();
		comanda.setDataComanda(new Date());
		comanda.adaugaElementComanda(cartiDinFisierText.get(0), 2);
		try {
			comanda.setDiscount(-0.4f);
			fail("Nu s-a generat exceptie pt discount mai mic decat 0");
		} catch (IllegalArgumentException illegalArgumentException) {
			System.out.println("S-a generat exceptie pt discount pt discount mai mic decat 0");
		}
		try {
			comanda.setDiscount(12.4f);
			fail("Nu s-a generat exceptie pt discount mai mare decat 1");
		} catch (IllegalArgumentException illegalArgumentException) {
			System.out.println("S-a generat exceptie pt discount mai mare decat 1");
		}
	}

	@Test
	public void testGetProdusFavorit() {
		Comanda comanda = new Comanda();
		comanda.setDataComanda(new Date());

		assertNull("Verificare get produs favorit pe lista carti goala", comanda.getProdusFavorit());

		comanda.adaugaElementComanda(cartiDinFisierText.get(0), 2);
		comanda.adaugaElementComanda(cartiDinFisierText.get(1), 100);

		assertNotNull("Verificare get produs favorit pe lista carti populata", comanda.getProdusFavorit());
		assertSame("Verificare get produs favorit pe lista carti populata", cartiDinFisierText.get(1),
				comanda.getProdusFavorit());
	}

	@Test
	public void testGenPreferat() {
		Comanda comanda = new Comanda();

		Carte carte1 =mock(Carte.class);
		Carte carte2 =mock(Carte.class);
		Carte carte3 =mock(Carte.class);
		when(carte1.getGen()).thenReturn("Fictiune");
		when(carte2.getGen()).thenReturn("Fictiune");
		when(carte3.getGen()).thenReturn("Bibliografie");
		comanda.adaugaElementComanda(carte1, 2);
		comanda.adaugaElementComanda(carte2, 3);
		comanda.adaugaElementComanda(carte3, 4);
		assertEquals("Fictiune", comanda.genPreferat());
		assertNotEquals("Istorie", comanda.genPreferat());
	}

	@Test
	public void testAdaugaElementComandaElementNull() {
		Comanda comanda = new Comanda();

		CarteAbstracta carteAbstracta = null;
		try {
			comanda.adaugaElementComanda(carteAbstracta, 2);
			fail("Se adauga la comanda si carti neprecizate(nule)");
		} catch (IllegalArgumentException exception) {
			System.out.println("S-a generat exceptie la incercarea de a adauga o carte nula la comanda.");
		}

	}

	@Test
	public void testGetNrProduseComandateListaNula() {
		Comanda comanda = new Comanda();
		comanda.setDataComanda(new Date());
		comanda.setListaCarti(null);
		assertEquals(0, comanda.getNrProduseComandate());
	}

}
