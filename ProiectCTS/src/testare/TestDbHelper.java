package testare;

import static org.junit.Assert.*;

import java.sql.Connection;

import org.junit.BeforeClass;
import org.junit.Test;

import clase.DbHelper;
import clase.SQLiteConnection;

public class TestDbHelper {

	private static DbHelper dbHelper;
	private static Connection connection;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		dbHelper = SQLiteConnection.getInstantaSQLiteConnection().getDbHelper();
		connection = SQLiteConnection.getInstantaSQLiteConnection().getConnection();
	}

	@Test
	public void testUpdateDisponibil() {
		dbHelper.updateDisponibil("978-606-594-130-4", connection, 40);
		int disponibilBd = dbHelper.getNrProduseDisponibileCuISBN("978-606-594-130-4", connection);
		assertEquals("Verificare modificari bd dupa apel update disponibil", disponibilBd, 40);
	}
	

}
