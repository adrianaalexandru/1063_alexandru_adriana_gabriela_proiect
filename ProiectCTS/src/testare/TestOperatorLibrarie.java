package testare;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clase.Adresa;
import clase.CartePrototypeFactory;
import clase.Client;
import clase.Comanda;
import clase.ComandaSpeciala;
import clase.LivrareLaDomiciliu;
import clase.OperatorLibrarie;
import clase.RidicareDeLaSediu;
import clase.Adresa.AdresaBuilder;
import clase_abstracte.CarteAbstracta;
import clase_abstracte.ComandaAbstracta;

public class TestOperatorLibrarie {

	private OperatorLibrarie operator1;
	private ComandaAbstracta comanda3;
	private ComandaAbstracta comanda1;
	private ComandaAbstracta comanda4;

	@Before
	public void setUp() throws Exception {
		operator1 = new OperatorLibrarie();

		Adresa adresa1 = new AdresaBuilder("915400", "A1", "Ap6").adaugaEtaj("2").adaugaJudet("Calarasi").build();
		Adresa adresa2 = new AdresaBuilder("915611", "B2", "Ap7").adaugaOras("Bucuresti").build();
		Adresa adresa3 = new AdresaBuilder("914775", "A12", "Ap14").build();

		Client client1 = new Client("Adriana Alexandru", "0723548515", "adriana.alexandru8@yahoo.com", adresa1);
		Client client2 = new Client("Elena Popescu", "0723455775", null, adresa2);
		Client client3 = new Client("Maria Ionescu", null, "maria.ionescu@yahoo.com", adresa3);

		CarteAbstracta carte1 = CartePrototypeFactory.getPrototip("978-606-594-130-4");
		CarteAbstracta carte2 = CartePrototypeFactory.getPrototip("978-973-46-2753-0");
		CarteAbstracta carte3 = CartePrototypeFactory.getPrototip("978-973-102-328-1");
		CarteAbstracta carte4 = CartePrototypeFactory.getPrototip("978-973-102-329-8");
		CarteAbstracta carte5 = CartePrototypeFactory.getPrototip("978-973-46-3680-8");
		CarteAbstracta carte6 = CartePrototypeFactory.getPrototip("978-606-8623-10-8");
		CarteAbstracta carte7 = CartePrototypeFactory.getPrototip("978-606-594-130-4");

		comanda1 = new Comanda();
		try {
			comanda1.adaugaElementComanda(carte1, 3);
			comanda1.adaugaElementComanda(carte2, 14);
		} catch (Exception e) {
			e.printStackTrace();
		}
		comanda1.setClient(client1);
		comanda1.setTipLivrare(new RidicareDeLaSediu());
		ComandaAbstracta comanda2 = new Comanda();
		try {
			comanda2.adaugaElementComanda(carte3, 12);
			comanda2.adaugaElementComanda(carte4, 13);
		} catch (Exception e) {
			e.printStackTrace();
		}
		comanda2.setClient(client2);
		comanda2.setTipLivrare(new RidicareDeLaSediu());
		comanda3 = new Comanda();
		try {
			comanda3.adaugaElementComanda(carte5, 11);
			comanda3.adaugaElementComanda(carte6, 15);
			comanda3.adaugaElementComanda(carte7, 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		comanda3.setClient(client3);
		comanda3.setTipLivrare(new LivrareLaDomiciliu());
		comanda4 = new ComandaSpeciala(comanda3);
		operator1.preiaComanda(comanda1);
		operator1.preiaComanda(comanda4);

	}

	@Test
	public void testVerificaComenziPreluatePentruProcesare() {
		
		assertNotEquals("Verificare comenzi preluate pt cazul in care comenzile nu depasesc 2 la nr", 0,
				operator1.getComenziPreluateDeOperator().size());

		operator1.preiaComanda(comanda3);
		assertEquals("Verificare comenzi preluate pt cazul in care comenzile depasesc 2 la nr", 0,
				operator1.getComenziPreluateDeOperator().size());
	}

}
