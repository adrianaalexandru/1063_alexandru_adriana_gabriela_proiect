package testare;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map.Entry;

import org.junit.Before;
import org.junit.Test;

import clase.Adresa;
import clase.Adresa.AdresaBuilder;
import clase.CartePrototypeFactory;
import clase.Client;
import clase.Comanda;
import clase.DbHelper;
import clase.OperatorLibrarie;
import clase.ProcesareComandaFacade;
import clase.Angajat;
import clase.RidicareDeLaSediu;
import clase.SQLiteConnection;
import clase_abstracte.CarteAbstracta;
import clase_abstracte.ComandaAbstracta;
import utils.Utils.STARE_COMANDA;

public class TestProcesareComanda {
	private ComandaAbstracta comanda;
	private Angajat realizatorComanda;

	@Before
	public void setUp() throws Exception {
		OperatorLibrarie operator1 = new OperatorLibrarie();
		Adresa adresa1 = new AdresaBuilder("915400", "A1", "Ap6").adaugaEtaj("2").adaugaJudet("Calarasi").build();
		Client client1 = new Client("Adriana Alexandru", "0723548515", "adriana.alexandru8@yahoo.com", adresa1);
		CarteAbstracta carte1 = CartePrototypeFactory.getPrototip("978-606-594-130-4");
		CarteAbstracta carte2 = CartePrototypeFactory.getPrototip("978-973-46-2753-0");

		comanda = new Comanda();
		try {
			comanda.adaugaElementComanda(carte1, 3);
			comanda.adaugaElementComanda(carte2, 14);
		} catch (Exception e) {
			e.printStackTrace();
		}
		comanda.setClient(client1);
		comanda.setTipLivrare(new RidicareDeLaSediu());

		realizatorComanda= new Angajat();
		comanda.setRealizatorComanda(realizatorComanda);
	}

	@Test
	public void testProceseazaComandaFacadeStareComanda() {
		ProcesareComandaFacade.proceseazaComanda(realizatorComanda, comanda);
		if (comanda.getRealizatorComanda().verificaStoc(comanda)) {
			assertEquals(STARE_COMANDA.In_curs_de_livrare, comanda.getStare());
		} else {
			assertEquals(STARE_COMANDA.Comanda_retrasa, comanda.getStare());
		}
	}

	@Test
	public void testVerificaStocCorectitudineRezultat() {
		assertTrue("Verificare rezultat functie verifica stoc pt stoc disponibil",
				comanda.getRealizatorComanda().verificaStoc(comanda));

		CarteAbstracta carte3 = CartePrototypeFactory.getPrototip("978-973-102-328-1");
		comanda.adaugaElementComanda(carte3, 14453);
		assertFalse("Verificare rezultat functie verifica stoc pt stoc indisponibil",
				comanda.getRealizatorComanda().verificaStoc(comanda));
	}

	@Test
	public void testActualizareBdDupaLivrareaProduselor() {
		Connection connection = clase.SQLiteConnection.getInstantaSQLiteConnection().getConnection();
		DbHelper dbHelper = SQLiteConnection.getInstantaSQLiteConnection().getDbHelper();

		HashMap<CarteAbstracta, Integer> hashMapProduseDisponibileInitial = new HashMap<>();
		for (Entry<CarteAbstracta, Integer> entry : comanda.getListaCarti().entrySet()) {
			CarteAbstracta produsDorit = entry.getKey();
			int nrProduseDispobile = dbHelper.getNrProduseDisponibileCuISBN(produsDorit.getISBN(), connection);
			hashMapProduseDisponibileInitial.put(produsDorit, nrProduseDispobile);
		}

		comanda.getRealizatorComanda().actualizareBdDupaLivrareaProduselor(comanda.getListaCarti());
		for (Entry<CarteAbstracta, Integer> entry : hashMapProduseDisponibileInitial.entrySet()) {
			CarteAbstracta produsDorit = entry.getKey();
			Integer nrProduseDispobile = entry.getValue();
			int nrProduseDispobilePrezent = dbHelper.getNrProduseDisponibileCuISBN(produsDorit.getISBN(), connection);
			assertEquals("Verificare modificari efectuate in bd",
					nrProduseDispobile - comanda.getListaCarti().get(produsDorit), nrProduseDispobilePrezent);
		}
	}

}
