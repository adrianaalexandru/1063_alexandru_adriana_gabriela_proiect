package testare;

import static org.junit.Assert.*;

import org.junit.Test;

import clase.Carte;
import clase.CartePrototypeFactory;
import clase_abstracte.CarteAbstracta;

public class TestCarte {

	@Test
	public void testSetNrPagini() {
		Carte carte = (Carte) CartePrototypeFactory.getPrototip("978-606-722-013-1");
		try {
			carte.setNrPagini(-100);
			fail("Nu s-a generat exceptie pt nr pagini negativ");
		} catch (IllegalArgumentException exception) {
			System.out.println("S-a generat exceptie pt nr pagini negativ!");
		}
	}

	@Test
	public void testSetPretNegativ() {
		Carte carte = (Carte) CartePrototypeFactory.getPrototip("978-606-722-013-1");
		try {
			carte.setPret(-100);
			fail("Nu s-a generat exceptie pt pret negativ");
		} catch (IllegalArgumentException exception) {
			System.out.println("S-a generat exceptie pt pret negativ!");
		}
	}

	@Test
	public void testSetISBNNull() {
		String ISBN = null;
		Carte carte = (Carte) CartePrototypeFactory.getPrototip("978-606-722-013-1");
		try {
			carte.setISBN(ISBN);
			fail("Nu s-a generat exceptie pt ISBN null");
		} catch (IllegalArgumentException exception) {
			System.out.println("S-a generat exceptie pt ISBN null!");
		}
	}

	@Test
	public void testSetISBNFormatGresit() {
		String ISBN = "128-2882-19118832";
		Carte carte = (Carte) CartePrototypeFactory.getPrototip("978-606-722-013-1");
		try {
			carte.setISBN(ISBN);
			fail("Nu s-a generat exceptie pt ISBN format gresit");
		} catch (IllegalArgumentException exception) {
			System.out.println("S-a generat exceptie pt ISBN format gresit!");
		}
	}

	@Test
	public void testSetISBNLungimeGresita() {
		String ISBN = "128";
		Carte carte = (Carte) CartePrototypeFactory.getPrototip("978-606-722-013-1");
		try {
			carte.setISBN(ISBN);
			fail("Nu s-a generat exceptie pt ISBN lungime gresita");
		} catch (IllegalArgumentException exception) {
			System.out.println("S-a generat exceptie pt ISBN lungime gresita!");
		}
	}

	@Test
	public void testSetISBNCorect() {
		String ISBN = "978-606-222-023-0";
		Carte carte = (Carte) CartePrototypeFactory.getPrototip("978-606-722-013-1");
		try {
			carte.setISBN(ISBN);
			assertEquals(ISBN, carte.getISBN());
		} catch (IllegalArgumentException exception) {
			fail("S-a generat exceptie pt ISBN corect");
		}
	}

	@Test
	public void testVerificaISBN() {		
		Carte carte1 = (Carte) CartePrototypeFactory.getPrototip("978-606-722-013-1");
		Carte carte2 = (Carte) CartePrototypeFactory.getPrototip("978-973-67-5625-2");
		Carte carte3 = (Carte) CartePrototypeFactory.getPrototip("978-973-50-3485-6");
		String isbnInvalid="978-973-50-3485-2";
		
		
		carte3.setISBN(isbnInvalid);
		assertTrue("Verificare ISBN valid",carte1.verificaISBN());
		assertTrue("Verificare ISBN valid",carte2.verificaISBN());
		assertFalse("Verificare ISBN invalid",carte3.verificaISBN());
	}

	@Test
	public void testEditorDinRomania() {
		String ISBNEditorRomania1 = "978-606-722-013-1";
		String ISBNEditorRomania2 = "978-973-88-7495-4";
		String ISBNEditorStrain = "978-616-8360-75-1";
		CarteAbstracta carte1 = CartePrototypeFactory.getPrototip(ISBNEditorRomania1);
		CarteAbstracta carte2 = CartePrototypeFactory.getPrototip(ISBNEditorRomania2);
		CarteAbstracta carte3 = CartePrototypeFactory.getPrototip(ISBNEditorStrain);

		assertTrue("Verificare editor din Romania caz pozitiv cu 606", carte1.editorDinRomania());
		assertTrue("Verificare editor din Romania caz pozitiv cu 973", carte2.editorDinRomania());
		assertFalse("Verificare editor din Romania caz negativ", carte3.editorDinRomania());
	}

}
