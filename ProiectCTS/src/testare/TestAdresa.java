package testare;

import static org.junit.Assert.*;

import org.junit.Test;

import clase.Adresa;
import clase.Adresa.AdresaBuilder;

public class TestAdresa {

	@Test
	public void testareAdresaBuilderCampuriNeinitializate() {
		Adresa adresa1 = new AdresaBuilder("915400", "A1", "3").build();

		assertEquals("Verificare camp neinitializat oras", "-", adresa1.getOras());
		assertEquals("Verificare camp neinitializat judet", "-", adresa1.getJudet());
		assertEquals("Verificare camp neinitializat etaj", "-", adresa1.getEtaj());
	}

	@Test
	public void testareReferinte() {
		AdresaBuilder adresaBuilder = new AdresaBuilder("915400", "A1", "3");
		Adresa adresa1 = adresaBuilder.build();
		Adresa adresa2 = adresaBuilder.adaugaEtaj("3").adaugaJudet("Calarasi").build();

		assertSame("Verificare referinte generate", adresa1, adresa2);
	}
	
	@Test
	public void testareAdresaBuilder() {
		AdresaBuilder adresaBuilder = new AdresaBuilder("915400", "A1", "3");
		Adresa adresa = adresaBuilder.adaugaEtaj("3").adaugaJudet("Calarasi").build();

		assertSame("Verificare camp etaj setat cu builder", "3", adresa.getEtaj());
		assertSame("Verificare camp judet setat cu builder", "Calarasi", adresa.getJudet());
	}

}
