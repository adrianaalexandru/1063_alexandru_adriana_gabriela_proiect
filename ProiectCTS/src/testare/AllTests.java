package testare;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({  TestAdresa.class, TestProcesareComanda.class, TestCarte.class,
		TestCartePrototypeFactory.class, TestClient.class, TestComanda.class, TestComandaSpeciala.class,
		TestDbHelper.class, TestEbook.class, TestOperatorLibrarie.class, TestSQLiteConnectionSingleton.class,
		TestUtils.class })
public class AllTests {

}
