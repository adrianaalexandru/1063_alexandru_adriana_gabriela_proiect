package testare;
import static org.junit.Assert.*;

import org.junit.Test;

import clase.CartePrototypeFactory;
import clase_abstracte.CarteAbstracta;

public class TestCartePrototypeFactory {

	@Test
	public void testareParametruNull() {
		assertNull(CartePrototypeFactory.getPrototip(null));
	}

	@Test
	public void testareCorectitudineImplememtarePrototype() {
		CarteAbstracta carte1=CartePrototypeFactory.getPrototip("978-606-594-130-4");
		CarteAbstracta carte2=CartePrototypeFactory.getPrototip("978-606-594-130-4");
		
		assertNotSame("Verificare obiecte generate de PrototypeFactory pt acelasi ISBN ",carte1,carte2);
	}
	
	@Test
	public void testarePrototipInexistent() {
		CarteAbstracta carte1=CartePrototypeFactory.getPrototip("abcde");
		
		assertNull(CartePrototypeFactory.getPrototip(null));
	}
}
