package testare;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import utils.Utils;

public class TestUtils {

	@Test
	public void testContineDoarLitere() {
		String expresiedeVerificat1 = "abcdefghij";
		String expresiedeVerificat2 = "abcde1222fghij";
		String expresiedeVerificat3 = "a";

		assertTrue(Utils.contineDoarLitere(expresiedeVerificat1));
		assertFalse(Utils.contineDoarLitere(expresiedeVerificat2));
		assertTrue(Utils.contineDoarLitere(expresiedeVerificat3));
	}

	@Test
	public void testContineDoarLitereParametruNull() {
		String expresiedeVerificat = null;

		try {
			boolean rezultat = Utils.contineDoarLitere(expresiedeVerificat);
			fail("Nu s-a generat eroare pt argument null test contine doar litere");
		} catch (IllegalArgumentException illegalArgumentException) {
			System.out.println("S-a generat eroare pt argument null test contine doar litere");
		}
	}
	
	@Test
	public void testContineDoarCifre() {
		String expresiedeVerificat1 = "12345678";
		String expresiedeVerificat2 = "12345abcd";
		String expresiedeVerificat3 = "1";
		String expresiedeVerificat4 = "111-!21";

		assertTrue(Utils.contineDoarCifre(expresiedeVerificat1));
		assertFalse(Utils.contineDoarCifre(expresiedeVerificat2));
		assertTrue(Utils.contineDoarCifre(expresiedeVerificat3));
		assertFalse(Utils.contineDoarCifre(expresiedeVerificat4));
	}

	@Test
	public void testContineDoarCifreParametruNull() {
		String expresiedeVerificat = null;

		try {
			boolean rezultat = Utils.contineDoarCifre(expresiedeVerificat);
			fail("Nu s-a generat eroare pt argument null test contine doar cifre");
		} catch (IllegalArgumentException illegalArgumentException) {
			System.out.println("S-a generat eroare pt argument null test contine doar cifre");
		}
	}
}
