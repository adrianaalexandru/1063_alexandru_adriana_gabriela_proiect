package testare;

import static org.junit.Assert.*;

import org.junit.Test;

import clase.SQLiteConnection;

public class TestSQLiteConnectionSingleton {

	@Test
	public void testareInstanteSQLiteConnection() {
		SQLiteConnection sqLiteConnection1 = SQLiteConnection.getInstantaSQLiteConnection();
		SQLiteConnection sqLiteConnection2 = SQLiteConnection.getInstantaSQLiteConnection();

		assertSame("Verificare singleton instante SQLiteConnection", sqLiteConnection1, sqLiteConnection2);
		assertSame("Verificare singleton instante Connection", sqLiteConnection1.getConnection(),
				sqLiteConnection2.getConnection());

	}

}
