package testare;

import static org.junit.Assert.*;

import javax.naming.OperationNotSupportedException;

import org.junit.Test;

import clase.Carte;
import clase.CartePrototypeFactory;
import clase.EBook;

public class TestEbook {

	@Test
	public void testSetFormatNull() {
		String format=null;
		EBook eBook=(EBook)CartePrototypeFactory.getPrototip("978-973-102-328-1");
		try{
			eBook.setFormat(format);
			fail("Formatul pt ebook accepta valoare nula!");
		}catch(OperationNotSupportedException exception){
			System.out.println("S-a generat exceptie la setarea unui format nul.");
		}
	}

	@Test
	public void testSetFormat() {
		String format="PDFS";
		EBook eBook=(EBook)CartePrototypeFactory.getPrototip("978-973-102-328-1");
		try{
			eBook.setFormat(format);
			fail("Formatul pt ebook accepta valoare diferita de PDF sau ePub!");
		}catch(OperationNotSupportedException exception){
			System.out.println("S-a generat exceptie la setarea unui format diferit de PDF sau ePub.");
		}
	}
	@Test
	public void testSetFormatValoriNormale() {
		String format="PDF";
		EBook eBook=(EBook)CartePrototypeFactory.getPrototip("978-973-102-328-1");
		try{
			eBook.setFormat(format);
			assertEquals(format, eBook.getFormat());
		}catch(OperationNotSupportedException exception){
			fail("Formatul pt ebook accepta valoare egala cu PDF!");
		}
		format="ePub";
		try{
			eBook.setFormat(format);
			assertEquals(format, eBook.getFormat());
		}catch(OperationNotSupportedException exception){
			fail("Formatul pt ebook accepta valoare egala cu ePub!");
		}
	}
}
