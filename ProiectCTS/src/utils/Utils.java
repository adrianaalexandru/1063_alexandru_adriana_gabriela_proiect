package utils;

import java.text.SimpleDateFormat;

public class Utils {
	public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");

	public enum STARE_COMANDA {
		Inregistrata, Preluata, Verificare_disponibilitate_articole, Comanda_confirmata, Comanda_retrasa, In_curs_de_livrare
	}

	public enum STARE_CLIENT_NOTIFICARE {
		Client_nenotificat, Client_notificat_prin_email, Client_notificat_prin_sms
	}

	public static boolean contineDoarLitere(String string) {
		if (string != null) {
			return string.matches("[a-zA-Z]+");
		} else {
			throw new IllegalArgumentException();
		}
	}

	public static boolean contineDoarCifre(String string) {
		if (string != null) {
			return string.matches("[0-9]+");
		} else {
			throw new IllegalArgumentException();
		}
	}
}
