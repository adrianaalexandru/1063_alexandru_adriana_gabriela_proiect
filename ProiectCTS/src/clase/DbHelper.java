package clase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map.Entry;

import javax.naming.OperationNotSupportedException;

import clase_abstracte.CarteAbstracta;
import clase_abstracte.ComandaAbstracta;
import utils.Utils;

public class DbHelper {

	public ArrayList<EBook> preiaToateEbookurileDinBd(Connection connection) {
		ArrayList<EBook> eBooks = new ArrayList<>();
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement("SELECT * FROM EBook");
			boolean rezultate = ps.execute();
			int rsCount = 0;

			// Loop through the available result sets.
			while (rezultate) {
				ResultSet rs = ps.getResultSet();

				while (rs.next()) {
					EBook eBookNou = new EBook();
					eBookNou.setISBN(rs.getString("ISBN"));
					eBookNou.setPret(rs.getFloat("pret"));
					eBookNou.setAutori(rs.getString("autori"));
					eBookNou.setGen(rs.getString("gen"));
					eBookNou.setTitlu(rs.getString("titlu"));
					eBookNou.setCompatibilitate(rs.getString("compatibilitate"));
					try {
						String format=rs.getString("format");
						eBookNou.setFormat(format);
					} catch (OperationNotSupportedException e) {
						e.printStackTrace();
					}
					eBooks.add(eBookNou);
				}

				rs.close();
				rezultate = ps.getMoreResults();
			}
			ps.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return eBooks;

	}

	public ArrayList<Carte> preiaToateCartileDinBd(Connection connection) {
		ArrayList<Carte> carti = new ArrayList<>();
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement("SELECT * FROM Carte");
			boolean rezultate = ps.execute();
			int rsCount = 0;

			while (rezultate) {
				ResultSet rs = ps.getResultSet();

				while (rs.next()) {
					Carte carteNoua = new Carte();
					carteNoua.setISBN(rs.getString("ISBN"));
					carteNoua.setPret(rs.getFloat("pret"));
					carteNoua.setAutori(rs.getString("autori"));
					carteNoua.setDimensiuni(rs.getString("dimensiuni"));
					carteNoua.setGen(rs.getString("gen"));
					carteNoua.setTitlu(rs.getString("titlu"));
					carteNoua.setNrPagini(rs.getInt("nrPagini"));
					carti.add(carteNoua);
				}
				rs.close();
				rezultate = ps.getMoreResults();
			}
			ps.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return carti;

	}

	public int getNrProduseDisponibileCuISBN(String ISBN, Connection connection) {
		PreparedStatement ps = null;
		int disponibil = 100;
		try {
			ps = connection.prepareStatement("SELECT * FROM Disponibil WHERE ISBN=?");
			ps.setString(1, ISBN);
			System.out.println("ISBN" + ISBN);
			ResultSet rs = ps.executeQuery();
			if (rs != null) {
				if (rs.next()) {
					disponibil = rs.getInt("cantitate_disponibila");
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return disponibil;
	}

	public void updateDisponibil(String isbn, Connection conn,int disponibilNou) {
		PreparedStatement change;
		try {
			change = conn.prepareStatement("update Disponibil set cantitate_disponibila = ? WHERE isbn=? ");
			change.setInt(1, disponibilNou);
			change.setString(2, isbn);
			change.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void adaugaComandaXCarteInBD(int idComanda, String ISBN, Connection connection) {
		try {
			String insertTableSQL = "INSERT INTO ComandaXCarte " + "(IDComanda, ISBN) VALUES" + "(?,?)";
			PreparedStatement preparedStatement = connection.prepareStatement(insertTableSQL);
			preparedStatement.setInt(1, idComanda);
			preparedStatement.setString(2, ISBN);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void adaugaComandaInBD(ComandaAbstracta comanda, Connection connection) {
		comanda.setIDComanda(getNextIdComanda(connection));
		PreparedStatement preparedStatement;
		try {
			String insertTableSQL = "INSERT INTO Comanda " + "(IDComanda, client, dataComanda,TipLivrare) VALUES"
					+ "(?,?,?,?)";
			preparedStatement = connection.prepareStatement(insertTableSQL);
			preparedStatement.setInt(1, comanda.getIDComanda());
			preparedStatement.setString(2, comanda.getClient().getNume());
			preparedStatement.setString(3, Utils.simpleDateFormat.format(comanda.getDataComanda()));
			int tipLivrare = 1;
			if (comanda.getTipLivrare().getClass().getName().equals("clase.RidicareDeLaSediu"))
				tipLivrare = 2;
			preparedStatement.setInt(4, tipLivrare);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		for (Entry<CarteAbstracta, Integer> entry : comanda.getListaCarti().entrySet()) {
			CarteAbstracta produsDorit = entry.getKey();

			adaugaComandaXCarteInBD(comanda.getIDComanda(), produsDorit.getISBN(), connection);
		}

	}

	private int getNextIdComanda(Connection connection) {
		int numberOfRows = 100;
		String query = "select count(*) from Comanda";
		PreparedStatement preparedStatement;
		try {
			preparedStatement = connection.prepareStatement(query);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				numberOfRows = resultSet.getInt(1);
			} else {
				System.out.println("error: could not get the record counts");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return numberOfRows + 1;
	}

	public Carte getCarte(String _ISBN, Connection con) {
		PreparedStatement ps = null;
		Carte carteAbstracta = new Carte();
		try {
			ps = con.prepareStatement("SELECT * FROM Carte WHERE ISBN=?");

			ps.setString(1, _ISBN);
			ResultSet rs = ps.executeQuery();
			carteAbstracta.setISBN(rs.getString("ISBN"));
			carteAbstracta.setPret(rs.getFloat("pret"));
			carteAbstracta.setAutori(rs.getString("autori"));
			carteAbstracta.setDimensiuni(rs.getString("dimensiuni"));
			carteAbstracta.setGen(rs.getString("gen"));
			carteAbstracta.setTitlu(rs.getString("titlu"));
			carteAbstracta.setNrPagini(rs.getInt("nrPagini"));

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return carteAbstracta;
	}

	public EBook getEbook(String _ISBN, Connection con) {
		PreparedStatement ps = null;
		EBook eBook = new EBook();
		try {
			ps = con.prepareStatement("SELECT * FROM EBook WHERE ISBN=?");

			ps.setString(1, _ISBN);
			ResultSet rs = ps.executeQuery();
			eBook.setISBN(_ISBN);
			eBook.setPret(rs.getFloat("pret"));
			eBook.setAutori(rs.getString("autori"));
			eBook.setGen(rs.getString("gen"));
			eBook.setTitlu(rs.getString("titlu"));
			eBook.setCompatibilitate(rs.getString("compatibilitate"));
			try {
				eBook.setFormat(rs.getString("format"));
			} catch (OperationNotSupportedException e) {
				e.printStackTrace();
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return eBook;
	}

	public void scriereBazadeDateEBook(EBook eBook, Connection con) {
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;

		try {
			ps2 = con.prepareStatement("SELECT COUNT(*) FROM EBook WHERE ISBN=?");
			ps2.setString(1, eBook.getISBN());
			ResultSet rs = ps2.executeQuery();
			int nrEbook = rs.getInt(1);
			if (nrEbook == 0) {
				try {
					ps = con.prepareStatement("INSERT INTO EBook VALUES(?,?,?,?,?,?,?);");
					ps.setString(1, eBook.getISBN());
					ps.setString(2, eBook.getTitlu());
					ps.setString(3, eBook.getAutori());
					ps.setString(4, eBook.getGen());
					ps.setFloat(5, eBook.getPret());
					ps.setString(6, eBook.getCompatibilitate());
					ps.setString(7, eBook.getFormat());
					ps.executeUpdate();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					try {
						ps.close();
					} catch (SQLException e) {

						e.printStackTrace();
					}

				}
			} else
				System.out.println("Exista deja un EBook cu ISBN-ul dat in baza de date!");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			ps2.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
