package clase;

import clase_abstracte.HandlerNotificare;
import clase_abstracte.Observabil;
import interfete.Observer;
import utils.Utils.STARE_CLIENT_NOTIFICARE;

public class ProcesatorNotificareEmail extends HandlerNotificare {

	@Override
	public void gestioneazaNotificare(Observer client, String mesaj) {
		if (((Client) client).getEmail() != null) {
			((Client)client).setStare(STARE_CLIENT_NOTIFICARE.Client_notificat_prin_email);
			client.update(((Client) client).getNume()+" Email la adresa "+((Client) client).getEmail()+" informare: " + mesaj);
		}else{
			((Client)client).setStare(STARE_CLIENT_NOTIFICARE.Client_nenotificat);
		}

	}

}
