package clase;

import java.util.Date;
import java.util.HashMap;

import clase_abstracte.CarteAbstracta;
import clase_abstracte.ComandaAbstracta;
import interfete.TipLivrare;
import utils.Utils;

public class DecoratorComanda extends ComandaAbstracta {

	private static final long serialVersionUID = 1L;
	protected final ComandaAbstracta comanda;

	public DecoratorComanda(ComandaAbstracta comanda) {
		this.comanda = comanda;
	}

	@Override
	public CarteAbstracta getProdusFavorit() {
		return comanda.getProdusFavorit();
	}

	@Override
	public void adaugaElementComanda(CarteAbstracta carteAbstracta, int nr) {
		comanda.adaugaElementComanda(carteAbstracta, nr);
	}

	public void setStare(Utils.STARE_COMANDA stare) {
		comanda.setStare(stare);
	}

	@Override
	public float valoareTotala() throws Exception {
		return comanda.valoareTotala();
	}

	@Override
	public String genPreferat() {
		return comanda.genPreferat();
	}

	@Override
	public void setDiscount(float discount) {
		if (discount < 0 || discount > 1) {
			throw new IllegalArgumentException();
		} else {
			comanda.setDiscount(discount);
		}
	}

	@Override
	public int getNrProduseComandate() {
		return comanda.getNrProduseComandate();
	}

	@Override
	public float getDiscount() {
		return comanda.getDiscount();
	}

	public TipLivrare getTipLivrare() {
		return comanda.getTipLivrare();
	}

	public void setTipLivrare(TipLivrare tipLivrare) {
		comanda.setTipLivrare(tipLivrare);
	}

	public Date getDataComanda() {
		return comanda.getDataComanda();
	}

	public HashMap<CarteAbstracta, Integer> getListaCarti() {
		return comanda.getListaCarti();
	}

	public void setListaCarti(HashMap<CarteAbstracta, Integer> listaCarti) {
		comanda.setListaCarti(listaCarti);
	}

	public void setDataComanda(Date date) {
		comanda.setDataComanda(date);
	}

	public Client getClient() {
		return comanda.getClient();
	}

	public void setClient(Client client) {
		if (comanda.getClient() != null) {
			comanda.stergeObserver(this.client);
		}
		comanda.setClient(client);
		comanda.adaugaObserver(client);
	}

	public Utils.STARE_COMANDA getStare() {
		return comanda.getStare();
	}

}
