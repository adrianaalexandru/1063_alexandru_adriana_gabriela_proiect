package clase;

import clase_abstracte.HandlerNotificare;
import interfete.Observer;
import utils.Utils.STARE_CLIENT_NOTIFICARE;

public class ProcesatorNotificareTelefon extends HandlerNotificare {

	@Override
	public void gestioneazaNotificare(Observer client, String mesaj) {
		if (((Client) client).getNrTelefon() != null) {
			((Client)client).setStare(STARE_CLIENT_NOTIFICARE.Client_notificat_prin_sms);
			client.update(((Client) client).getNume()+" SMS informare la nr de telefon "+((Client) client).getNrTelefon() +":"+ mesaj);
		} else {
			succesor.gestioneazaNotificare(client, mesaj);
		}

	}

}
