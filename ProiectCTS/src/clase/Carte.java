package clase;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Scanner;

import clase_abstracte.CarteAbstracta;

public class Carte extends CarteAbstracta implements Serializable {
	private int nrPagini;
	private String dimensiuni;

	// metode
	public String getDimensiuni() {
		return dimensiuni;
	}

	public Carte(String iSBN, String titlu, String autori, String gen, float pret, int nrPagini, String dimensiuni) {
		super(iSBN, titlu, autori, gen, pret);
		this.nrPagini = nrPagini;
		this.dimensiuni = dimensiuni;
	}

	public Carte() {
		super();
		this.nrPagini = 0;
		this.dimensiuni = "0x0";
	}

	public void setDimensiuni(String _dimensiuni) {
		dimensiuni = _dimensiuni;
	}

	public int getNrPagini() {
		return nrPagini;
	}

	public void setNrPagini(int _nrPagini) {
		if (_nrPagini > 0) {
			this.nrPagini = _nrPagini;
		} else {
			throw new IllegalArgumentException();
		}
	}

	public String toString() {

		return super.toString() + this.nrPagini + " " + this.dimensiuni;
	}

	public void scriereFisierTextCarte(BufferedWriter bw) {
		super.scriereFisierTextCarteAbstracta(bw);
		try {
			bw.write(nrPagini + "\t");
			bw.write(dimensiuni + "\t");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void citireFisierTextCarte(Scanner scan) {
		super.citireFisierTextCarteAbstracta(scan);
		this.setNrPagini(scan.nextInt());
		this.setDimensiuni(scan.next());
	}

}
