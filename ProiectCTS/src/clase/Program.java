package clase;

import clase.Adresa.AdresaBuilder;
import clase_abstracte.CarteAbstracta;
import clase_abstracte.ComandaAbstracta;

public class Program {

	public static void main(String[] args) {
		OperatorLibrarie operator1 = new OperatorLibrarie();

		// builder
		Adresa adresa1 = new AdresaBuilder("915400", "A1", "Ap6").adaugaEtaj("2").adaugaJudet("Calarasi").build();
		Adresa adresa2 = new AdresaBuilder("915611", "B2", "Ap7").adaugaOras("Bucuresti").build();
		Adresa adresa3 = new AdresaBuilder("914775", "A12", "Ap14").build();

		Client client1 = new Client("Adriana Alexandru", "0723548515", "adriana.alexandru8@yahoo.com", adresa1);
		Client client2 = new Client("Elena Popescu", "0723455775", null, adresa2);
		Client client3 = new Client("Maria Ionescu", null, "maria.ionescu@yahoo.com", adresa3);

		// prototype
		CarteAbstracta carte1 = CartePrototypeFactory.getPrototip("978-606-594-130-4");
		CarteAbstracta carte2 = CartePrototypeFactory.getPrototip("978-973-46-2753-0");
		CarteAbstracta carte3 = CartePrototypeFactory.getPrototip("978-973-102-328-1");
		CarteAbstracta carte4 = CartePrototypeFactory.getPrototip("978-973-102-329-8");
		CarteAbstracta carte5 = CartePrototypeFactory.getPrototip("978-973-46-3680-8");
		CarteAbstracta carte6 = CartePrototypeFactory.getPrototip("978-606-8623-10-8");
		CarteAbstracta carte7 = CartePrototypeFactory.getPrototip("978-606-594-130-4");
		if (!carte1.equals(carte7)) {
			System.out.println("Prototype corect implementat!");
		}

		ComandaAbstracta comanda1 = new Comanda();
		try {
			comanda1.adaugaElementComanda(carte1, 3);
			comanda1.adaugaElementComanda(carte2, 14);
		} catch (Exception e) {
			e.printStackTrace();
		}
		comanda1.setClient(client1);
		comanda1.setTipLivrare(new RidicareDeLaSediu());
		ComandaAbstracta comanda2 = new Comanda();
		try {
			comanda2.adaugaElementComanda(carte3, 12);
			comanda2.adaugaElementComanda(carte4, 13);
		} catch (Exception e) {
			e.printStackTrace();
		}
		comanda2.setClient(client2);
		comanda2.setTipLivrare(new RidicareDeLaSediu());
		ComandaAbstracta comanda3 = new Comanda();
		try {
			comanda3.adaugaElementComanda(carte5, 11);
			comanda3.adaugaElementComanda(carte6, 15);
			comanda3.adaugaElementComanda(carte7, 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		comanda3.setClient(client3);
		comanda3.setTipLivrare(new LivrareLaDomiciliu());
		ComandaAbstracta comanda4 = new ComandaSpeciala(comanda3);

		operator1.preiaComanda(comanda1);
		operator1.preiaComanda(comanda4);
		operator1.preiaComanda(comanda3);
		
		
		//adaptor
		AdaptorComanda adaptorComanda=new AdaptorComanda(comanda1);
		String genpref=comanda1.genPreferat();
		String genprefAdaptor=adaptorComanda.favoriteGenre();
		System.out.println(genpref+" "+genprefAdaptor);
		adaptorComanda.addNewCommandElement(carte3, 10);
	}

}
