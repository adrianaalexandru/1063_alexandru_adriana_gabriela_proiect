package clase;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Scanner;

import javax.naming.OperationNotSupportedException;

import clase_abstracte.CarteAbstracta;

public class EBook extends CarteAbstracta implements Serializable {
	private String compatibilitate;
	private String format;

	// metode
	public EBook(String iSBN, String titlu, String autori, String gen, float pret, String compatibilitate,
			String format) {
		super(iSBN, titlu, autori, gen, pret);
		this.compatibilitate = compatibilitate;
		this.format = format;
	}

	public EBook() {
		super();
		this.compatibilitate = "-";
		this.format = null;
	}

	public String toString() {
		return super.toString() + " " + this.compatibilitate + " " + this.format;
	}

	public String getCompatibilitate() {
		return compatibilitate;
	}

	public void setCompatibilitate(String compatibilitate) {
		this.compatibilitate = compatibilitate;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) throws OperationNotSupportedException {
		if (format == null || ((format.compareTo("ePub") != 0) && (format.compareTo("PDF") != 0))) {
			throw new OperationNotSupportedException();
		} else {
			this.format = format;
		}
	}

	public void scriereFisierTextEBook(BufferedWriter bw) {
		super.scriereFisierTextCarteAbstracta(bw);
		try {
			bw.write(this.compatibilitate + "\t");
			bw.write(this.format + "\t");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void citireFisierTextEBook(Scanner scan) {
		super.citireFisierTextCarteAbstracta(scan);
		this.setCompatibilitate(scan.next());
		this.format = scan.next();
	}


}
