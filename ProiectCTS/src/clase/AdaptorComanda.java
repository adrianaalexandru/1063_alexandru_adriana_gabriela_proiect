package clase;

import clase_abstracte.CarteAbstracta;
import clase_abstracte.ComandaAbstracta;
import interfete.ICommand;

public class AdaptorComanda implements ICommand {

	private ComandaAbstracta comanda;

	public AdaptorComanda(ComandaAbstracta comanda) {
		this.comanda = comanda;
	}

	@Override
	public float totalValue() throws Exception {
		return comanda.valoareTotala();
	}

	@Override
	public String favoriteGenre() {
		return comanda.genPreferat();
	}

	@Override
	public void addNewCommandElement(CarteAbstracta carteAbstracta, int nr) {
		comanda.adaugaElementComanda(carteAbstracta, nr);
	}

	@Override
	public void notifyStateChange(ComandaAbstracta comandaAbstracta) {
		comanda.notificaSchimbareStare(comandaAbstracta);

	}

}
