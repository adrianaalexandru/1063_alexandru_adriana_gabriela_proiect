package clase;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import clase_abstracte.CarteAbstracta;
import clase_abstracte.ComandaAbstracta;
import interfete.TipLivrare;
import utils.Utils;

public class Comanda extends ComandaAbstracta implements Serializable {

	private static final long serialVersionUID = 1L;

	public Comanda() {
		this.dataComanda = new Date();
	}

	public float getPretMediuProdus() {
		if ((listaCarti != null) && listaCarti.size() != 0) {
			int valoare = 0;
			for (Map.Entry<CarteAbstracta, Integer> element : listaCarti.entrySet()) {
				valoare += element.getKey().getPret();
			}
			return valoare / listaCarti.size();
		} else
			return 0;
	}

	@Override
	public int getNrProduseComandate() {
		int nrProduse = 0;
		if ( listaCarti != null) {
			for (Map.Entry<CarteAbstracta, Integer> element : listaCarti.entrySet()) {
				nrProduse += element.getValue();
			}
		}
		return nrProduse;
	}

	@Override
	public void setDiscount(float discount) {
		if (discount < 0 || discount > 1) {
			throw new IllegalArgumentException();
		} else {
			this.setDiscount(discount);
		}
	}

	@Override
	public float getDiscount() {
		return this.discount;
	}


	public void setStare(Utils.STARE_COMANDA stare) {
		this.stare = stare;
		notificaSchimbareStare(this);
	}

	@Override
	public void adaugaElementComanda(CarteAbstracta carteAbstracta, int nr) {
		if ((carteAbstracta != null) && (nr > 0)) {
			if (listaCarti == null) {
				listaCarti = new HashMap<>();
			}
			listaCarti.put(carteAbstracta, nr);
		} else
			throw new IllegalArgumentException("Cantitate negativa sau carte neprecizata!");
	}

	@Override
	public CarteAbstracta getProdusFavorit() {
		CarteAbstracta carteAbstracta = null;
		int max = -1;
		for (Map.Entry<CarteAbstracta, Integer> element : listaCarti.entrySet()) {
			if (max < element.getValue()) {
				carteAbstracta = element.getKey();
				max = element.getValue();
			}
		}
		return carteAbstracta;
	}

	@Override
	public float valoareTotala() throws Exception {
		if (listaCarti != null) {
			float valoare = 0;
			for (Map.Entry<CarteAbstracta, Integer> element : listaCarti.entrySet()) {
				valoare += element.getKey().getPret() * element.getValue();
			}
			return valoare;
		} else {
			throw new Exception("Lista nula");
		}
	}

	public String genPreferat() {
		if (listaCarti != null) {
			HashMap<String, Integer> nrGenuri = new HashMap<String, Integer>();
			System.out.println("lista carti " + listaCarti.size());
			for (Entry<CarteAbstracta, Integer> element : listaCarti.entrySet()) {
				if (nrGenuri.containsKey(element.getKey().getGen()))
					nrGenuri.put(element.getKey().getGen(), nrGenuri.get(element.getKey().getGen()) + 1);
				else
					nrGenuri.put(element.getKey().getGen(), 1);
			}
			int max = 0;
			String maxGen = "";
			for (Entry<String, Integer> gen : nrGenuri.entrySet()) {
				if (max < gen.getValue()) {
					max = gen.getValue();
					maxGen = gen.getKey();
				}
			}
			return maxGen;
		} else
			return null;
	}


	public void livreazaComanda() {
		tipLivrare.livreazaComanda(this);
		float val;
		try {
			val = valoareTotala();
			if (tipLivrare.getClass().getName().equals("clase.RidicareDeLaSediu")) {
				notifica("Comanda dvs. in valoare de " + val + " se afla la sediu si poate fi ridicata!");
			} else {
				notifica("Comanda dvs. in valoare de " + val + " va fi livrata la domiciu in cel mai scurt timp!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public TipLivrare getTipLivrare() {
		return tipLivrare;
	}

	public void setTipLivrare(TipLivrare tipLivrare) {
		this.tipLivrare = tipLivrare;
	}

	public Date getDataComanda() {
		return dataComanda;
	}

	public HashMap<CarteAbstracta, Integer> getListaCarti() {
		return listaCarti;
	}

	public void setListaCarti(HashMap<CarteAbstracta, Integer> listaCarti) {
		this.listaCarti = listaCarti;
	}

	@Override
	public void setDataComanda(Date date) {
		this.dataComanda = date;
	}

	@Override
	public Client getClient() {
		return client;
	}

	@Override
	public void setClient(Client client) {
		if (this.client != null) {
			this.stergeObserver(this.client);
		}
		this.client = client;
		this.adaugaObserver(client);
	}

	@Override
	public Utils.STARE_COMANDA getStare() {
		return stare;
	}
	
}
