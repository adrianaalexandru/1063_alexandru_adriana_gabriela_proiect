package clase;

import java.util.Date;
import java.util.Iterator;
import java.util.Vector;

import clase_abstracte.ComandaAbstracta;

public class OperatorLibrarie {
	private String nume;
	private Vector<ComandaAbstracta> comenziPreluateDeOperator = new Vector<ComandaAbstracta>();

	public OperatorLibrarie() {
		this.nume = nume;
	}

	public Vector<ComandaAbstracta> getComenziPreluateDeOperator() {
		return comenziPreluateDeOperator;
	}

	public void preiaComanda(ComandaAbstracta comandaNoua) {
		comandaNoua.setDataComanda(new Date());
		if (this.comenziPreluateDeOperator == null)
			this.comenziPreluateDeOperator = new Vector<>();
		this.comenziPreluateDeOperator.addElement(comandaNoua);
		verificaComenziPreluatePentruProcesare();
	}

	public void verificaComenziPreluatePentruProcesare() {
		if (comenziPreluateDeOperator.size() > 2) {
			Iterator<ComandaAbstracta> iterator = comenziPreluateDeOperator.iterator();
			while (iterator.hasNext()) {
				ComandaAbstracta comandaAbstracta = iterator.next();
				comandaAbstracta.setRealizatorComanda(new Angajat());
				try {
					comandaAbstracta.getRealizatorComanda().preiaComanda(comandaAbstracta);
				} catch (Exception e) {
					e.printStackTrace();
				}
				iterator.remove();
			}
		}

	}
}
