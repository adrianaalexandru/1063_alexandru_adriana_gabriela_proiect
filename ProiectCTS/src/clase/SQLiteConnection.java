package clase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SQLiteConnection {
	private Connection conn;
	private DbHelper dbHelper;
	private static SQLiteConnection instantaSQLiteConnection = null;

	public static SQLiteConnection getInstantaSQLiteConnection() {
		if (instantaSQLiteConnection == null) {
			instantaSQLiteConnection = new SQLiteConnection();
		}
		return instantaSQLiteConnection;
	}

	private SQLiteConnection() {
		conectare();
		setDbHelper(new DbHelper());
	}

	public Connection getConnection() {
		return conn;
	}

	public void conectare() {
		try {
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection("jdbc:sqlite:D:\\Projects\\JavaProjects\\ProiectJava\\DBCarti.sqlite");

		} catch (ClassNotFoundException e) {
			e.printStackTrace();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public DbHelper getDbHelper() {
		return dbHelper;
	}

	public void setDbHelper(DbHelper dbHelper) {
		this.dbHelper = dbHelper;
	}

}
