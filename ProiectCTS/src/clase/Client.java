package clase;

import interfete.Observer;
import utils.Utils;
import utils.Utils.STARE_CLIENT_NOTIFICARE;

public class Client implements Observer {

	private String nume;
	private String nrTelefon;
	private String email;
	private Adresa adresa;
	private STARE_CLIENT_NOTIFICARE stare;

	public Client(String nume, String nrTelefon, String email, Adresa adresa) {
		super();
		this.nume = nume;
		this.nrTelefon = nrTelefon;
		this.email = email;
		this.adresa = adresa;
		this.stare = STARE_CLIENT_NOTIFICARE.Client_nenotificat;
	}

	public Client(String nume) {
		this.nume = nume;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		if ((nume != null) && (Utils.contineDoarLitere(nume)) && nume.length() > 4) {
			this.nume = nume;
		} else {
			throw new UnsupportedOperationException();
		}
	}

	public String getNrTelefon() {
		return nrTelefon;
	}

	public void setNrTelefon(String nrTelefon) {
		if ((nrTelefon != null) && (Utils.contineDoarCifre(nrTelefon)) && nrTelefon.length() > 10){
			this.nrTelefon = nrTelefon;
		} else {
			throw new UnsupportedOperationException();
		}
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		if ((email != null) && (!email.contains("@") && email.length() > 3)) {
			this.email = email;
		} else {
			throw new UnsupportedOperationException();
		}
	}

	public Adresa getAdresa() {
		return adresa;
	}

	public void setAdresa(Adresa adresa) {
		this.adresa = adresa;
	}

	@Override
	public void update(String mesaj) {
		System.out.println(mesaj);
	}

	public STARE_CLIENT_NOTIFICARE getStare() {
		return stare;
	}

	public void setStare(STARE_CLIENT_NOTIFICARE stare) {
		this.stare = stare;
	}
}
