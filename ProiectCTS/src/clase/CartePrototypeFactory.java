package clase;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import clase_abstracte.CarteAbstracta;

public class CartePrototypeFactory {
	private static HashMap<String, CarteAbstracta> prototipuriCarti = new HashMap<>();

	static {
		Connection connection = SQLiteConnection.getInstantaSQLiteConnection().getConnection();

		
		ArrayList<EBook> eBooksBD = SQLiteConnection.getInstantaSQLiteConnection().getDbHelper()
				.preiaToateEbookurileDinBd(connection);
		ArrayList<Carte> cartiBD = SQLiteConnection.getInstantaSQLiteConnection().getDbHelper()
				.preiaToateCartileDinBd(connection);
		if (eBooksBD != null)
			for (int i = 0; i < eBooksBD.size(); i++) {
				prototipuriCarti.put(eBooksBD.get(i).getISBN(), eBooksBD.get(i));
			}
		if (cartiBD != null)
			for (int i = 0; i < cartiBD.size(); i++) {
				prototipuriCarti.put(cartiBD.get(i).getISBN(), cartiBD.get(i));
			}

	}

	public static CarteAbstracta getPrototip(String isbn) {
		CarteAbstracta copiePrototip = null;
		CarteAbstracta prototip = prototipuriCarti.get(isbn);
		if (prototip != null)
			copiePrototip = (CarteAbstracta) prototip.clone();
		return copiePrototip;
	}
}
