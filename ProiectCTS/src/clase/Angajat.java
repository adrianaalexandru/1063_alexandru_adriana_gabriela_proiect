package clase;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import clase_abstracte.CarteAbstracta;
import clase_abstracte.ComandaAbstracta;
import utils.Utils;
import utils.Utils.STARE_COMANDA;

public class Angajat {

	public static File fisierComenziPreluate = new File("comenziPreluate.txt");
	public static File fisierComenziNeonorate = new File("comenziNeonorate.txt");

	public String nume = "Anonim";

	public String getNume() {
		return this.nume;
	}

	public void adaugareComandaInBd(ComandaAbstracta comanda) {
		if (comanda != null) {
			DbHelper dbHelper = SQLiteConnection.getInstantaSQLiteConnection().getDbHelper();
			Connection connection = SQLiteConnection.getInstantaSQLiteConnection().getConnection();

			dbHelper.adaugaComandaInBD(comanda, connection);
		}
	}

	public void salvareComandaPreluataInFisiereInterne(ComandaAbstracta comandaAbstracta, File fisier) {
		try {
			FileWriter fw = new FileWriter(fisier);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(comandaAbstracta.getClient().getNume() + "\t");
			bw.write(Utils.simpleDateFormat.format(comandaAbstracta.getDataComanda()) + "\t");
			bw.write(comandaAbstracta.getListaCarti().size() + "\t");
			for (Map.Entry<CarteAbstracta, Integer> element : comandaAbstracta.getListaCarti().entrySet()) {

				if (element.getKey().getClass().getName().equals("clase.EBook")) {
					bw.write(0 + "\t");
					EBook eb = (EBook) element.getKey();
					eb.scriereFisierTextEBook(bw);
				} else {
					bw.write(1 + "\t");
					Carte c = (Carte) element.getKey();
					c.scriereFisierTextCarte(bw);
				}
			}
			bw.close();
			fw.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void actualizareBdDupaLivrareaProduselor(HashMap<CarteAbstracta, Integer> produseDeScosDinStoc) {
		DbHelper dbHelper = SQLiteConnection.getInstantaSQLiteConnection().getDbHelper();
		Connection connection = SQLiteConnection.getInstantaSQLiteConnection().getConnection();

		for (Entry<CarteAbstracta, Integer> entry : produseDeScosDinStoc.entrySet()) {
			CarteAbstracta produsDorit = entry.getKey();
			Integer nrProduseDorite = entry.getValue();

			int nrProduseDispobile = dbHelper.getNrProduseDisponibileCuISBN(produsDorit.getISBN(), connection);
			int nrProduseRamase = nrProduseDispobile - nrProduseDorite;
			dbHelper.updateDisponibil(produsDorit.getISBN(), connection, nrProduseRamase);
		}
	}

	public void emitereBonFiscalComanda(ComandaAbstracta comandaAbstracta) {
		try {
			System.out
					.println("Emitere bon fiscal asociat comenzii  in valoare de " + comandaAbstracta.valoareTotala());
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	public void preiaComanda(ComandaAbstracta comanda) {
		comanda.setStare(STARE_COMANDA.Preluata);
		ProcesareComandaFacade.proceseazaComanda(this, comanda);
	}

	public boolean verificaStoc(ComandaAbstracta comanda) {
		boolean disponibil = true;
		DbHelper dbHelper = SQLiteConnection.getInstantaSQLiteConnection().getDbHelper();
		Connection connection = SQLiteConnection.getInstantaSQLiteConnection().getConnection();

		for (Entry<CarteAbstracta, Integer> entry : comanda.getListaCarti().entrySet()) {
			CarteAbstracta produsDorit = entry.getKey();
			Integer nrProduseDorite = entry.getValue();

			int nrProduseDisponibile = dbHelper.getNrProduseDisponibileCuISBN(produsDorit.getISBN(), connection);
			if (nrProduseDorite > nrProduseDisponibile) {
				disponibil = false;
			}
		}
		return disponibil;
	}
}
