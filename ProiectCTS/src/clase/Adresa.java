package clase;

public class Adresa {

	private String oras;
	private String judet;
	private String codPostal;
	private String bloc;
	private String apartament;
	private String etaj;

	private Adresa(String codPostal, String bloc, String apartament) {

		this.codPostal = codPostal;
		this.bloc = bloc;
		this.apartament = apartament;
		this.oras = "-";
		this.judet = "-";
		this.etaj = "-";
	}

	public String getOras() {
		return oras;
	}

	public String getJudet() {
		return judet;
	}

	public String getCodPostal() {
		return codPostal;
	}

	public String getBloc() {
		return bloc;
	}

	public String getApartament() {
		return apartament;
	}

	public String getEtaj() {
		return etaj;
	}

	public static class AdresaBuilder {

		private Adresa adresa;

		public AdresaBuilder(String codPostal, String bloc, String apartament) {
			this.adresa = new Adresa(codPostal, bloc, apartament);
		}

		public AdresaBuilder adaugaOras(String oras) {
			adresa.oras = oras;
			return this;
		}

		public AdresaBuilder adaugaJudet(String judet) {
			adresa.judet = judet;
			return this;
		}

		public AdresaBuilder adaugaEtaj(String etaj) {
			adresa.etaj = etaj;
			return this;
		}

		public Adresa build() {
			return adresa;
		}
	}

}
