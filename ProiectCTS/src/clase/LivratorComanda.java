package clase;

import clase_abstracte.ComandaAbstracta;
import utils.Utils.STARE_COMANDA;

public class LivratorComanda {
	public String nume = "Anonim";

	public String getNume() {
		return this.nume;
	}
	
	
	public void livreazaComanda(ComandaAbstracta comanda){
		System.out.println("Se incepe livrarea comenzii");
		comanda.setStare(STARE_COMANDA.In_curs_de_livrare);
		comanda.getTipLivrare().livreazaComanda(comanda);
	}
}
