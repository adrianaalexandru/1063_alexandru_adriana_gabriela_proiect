package clase;

import clase_abstracte.ComandaAbstracta;
import utils.Utils.STARE_COMANDA;

public class ProcesareComandaFacade {

	public static void proceseazaComanda(Angajat angajat,ComandaAbstracta comanda) {
		angajat.salvareComandaPreluataInFisiereInterne(comanda, Angajat.fisierComenziPreluate);

		comanda.setStare(STARE_COMANDA.Verificare_disponibilitate_articole);
		if (angajat.verificaStoc(comanda)) {
			comanda.setStare(STARE_COMANDA.Comanda_confirmata);
			angajat.adaugareComandaInBd(comanda);
			angajat.emitereBonFiscalComanda(comanda);
			LivratorComanda livratorComanda=new LivratorComanda();
			livratorComanda.livreazaComanda(comanda);	
		} else {
			comanda.setStare(STARE_COMANDA.Comanda_retrasa);
			angajat.salvareComandaPreluataInFisiereInterne(comanda, Angajat.fisierComenziNeonorate);
		}
	}
}
