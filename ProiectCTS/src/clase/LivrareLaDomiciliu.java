package clase;

import clase_abstracte.ComandaAbstracta;
import interfete.TipLivrare;

public class LivrareLaDomiciliu implements TipLivrare {

	@Override
	public void livreazaComanda(ComandaAbstracta comanda) {
		System.out.println(" Livrare comanda la domiciliu.");
		
	}

}
