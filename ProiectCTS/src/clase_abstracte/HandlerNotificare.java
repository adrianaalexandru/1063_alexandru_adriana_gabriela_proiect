package clase_abstracte;

import interfete.Observer;

public abstract class HandlerNotificare {
	protected HandlerNotificare succesor = null;

	public void setSuccessor(HandlerNotificare Succesor) {
		succesor = Succesor;
	}

	public abstract void gestioneazaNotificare(Observer client, String mesaj);

}
