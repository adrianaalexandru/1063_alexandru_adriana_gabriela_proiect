package clase_abstracte;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import clase.Client;
import clase.Angajat;
import interfete.TipLivrare;
import utils.Utils;
import utils.Utils.STARE_COMANDA;

public abstract class ComandaAbstracta extends Observabil implements Serializable {
	private int IDComanda;
	protected Client client;
	protected Date dataComanda;
	private Angajat realizatorComanda;
	protected HashMap<CarteAbstracta, Integer> listaCarti = new HashMap<CarteAbstracta, Integer>();
	protected TipLivrare tipLivrare;
	protected Utils.STARE_COMANDA stare;
	protected float discount;

	public abstract void setDiscount(float discount);

	public abstract float getDiscount();
	
	public abstract int getNrProduseComandate();

	public abstract float valoareTotala() throws Exception;

	public abstract String genPreferat();

	public abstract CarteAbstracta getProdusFavorit();

	public abstract void adaugaElementComanda(CarteAbstracta carteAbstracta, int nr);

	public abstract Client getClient();

	public abstract STARE_COMANDA getStare();

	public abstract void setClient(Client client);

	public abstract TipLivrare getTipLivrare();

	public abstract void setTipLivrare(TipLivrare tipLivrare);

	public abstract Date getDataComanda();

	public abstract HashMap<CarteAbstracta, Integer> getListaCarti();

	public abstract void setListaCarti(HashMap<CarteAbstracta, Integer> listaCarti);

	public abstract void setDataComanda(Date date);

	public abstract void setStare(Utils.STARE_COMANDA stare);

	public void notificaSchimbareStare(ComandaAbstracta comandaAbstracta) {
		switch (stare) {
		case Comanda_confirmata:
			notifica("Comanda dvs din data de " + Utils.simpleDateFormat.format(comandaAbstracta.dataComanda)
					+ " a fost confirmata.");
			break;
		case In_curs_de_livrare:
			notifica("Comanda dvs din data de " + Utils.simpleDateFormat.format(comandaAbstracta.dataComanda)
					+ " este in curs de livrare.");
			break;
		case Verificare_disponibilitate_articole:
			notifica("Se verifica disponibilitatea articolelor pt comanda dvs din data de "
					+ Utils.simpleDateFormat.format(comandaAbstracta.dataComanda));
			break;
		case Inregistrata:
			notifica("Comanda dvs din data de " + Utils.simpleDateFormat.format(comandaAbstracta.dataComanda)
					+ " a fost inregistrata.");
			break;
		case Comanda_retrasa:
			notifica("Comanda dvs din data de " + Utils.simpleDateFormat.format(comandaAbstracta.dataComanda)
					+ " a fost retrasa.Articolele comandate nu sunt disponibile in acest moment.");
			break;
		case Preluata:
			notifica("Comanda dvs din data de " + Utils.simpleDateFormat.format(comandaAbstracta.dataComanda)
					+ " a fost preluata de unul din operatorii nostri.");
			break;
		default:
			break;
		}
	}

	public int getIDComanda() {
		return IDComanda;
	}

	public void setIDComanda(int iDComanda) {
		IDComanda = iDComanda;
	}

	public Angajat getRealizatorComanda() {
		return realizatorComanda;
	}

	public void setRealizatorComanda(Angajat realizatorComanda) {
		this.realizatorComanda = realizatorComanda;
	}

}
