package clase_abstracte;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Scanner;

public abstract class CarteAbstracta implements Serializable, Cloneable {
	protected String ISBN;
	protected String titlu;
	protected String autori;
	protected String gen;
	protected float pret;

	public CarteAbstracta(String iSBN, String titlu, String autori, String gen, float pret) {
		this.ISBN = iSBN;
		this.titlu = titlu;
		this.autori = autori;
		this.gen = gen;
		this.pret = pret;
	}

	public CarteAbstracta() {
		this.ISBN = "-";
		this.titlu = "-";
		this.autori = "-";
		this.gen = "-";
		this.pret = 0;
	}

	public String getISBN() {
		return ISBN;
	}

	public boolean editorDinRomania() {
		boolean editorRomania = false;
		if (this.ISBN != null) {
			if (this.ISBN.split("-")[1].equals("973") || this.ISBN.split("-")[1].equals("606"))
				editorRomania = true;
		}
		return editorRomania;
	}

	public boolean validateIsbn13(String isbn) {
		if (isbn == null) {
			return false;
		}

		isbn = isbn.replaceAll("-", "");
		if (isbn.length() != 13) {
			return false;
		}

		try {
			int tot = 0;
			for (int i = 0; i < 12; i++) {
				int digit = Integer.parseInt(isbn.substring(i, i + 1));
				tot += (i % 2 == 0) ? digit * 1 : digit * 3;
			}

			int sumaVerif = 10 - (tot % 10);
			if (sumaVerif == 10) {
				sumaVerif = 0;
			}

			return sumaVerif == Integer.parseInt(isbn.substring(12));
		} catch (NumberFormatException nfe) {
			return false;
		}
	}

	public boolean verificaISBN() {
		String isbn;
		if (this.ISBN == null) {
			return false;
		}

		isbn = this.ISBN.replaceAll("-", "");
		if (isbn.length() != 13) {
			return false;
		}

		try {
			int tot = 0;
			for (int i = 0; i < 12; i++) {
				int digit = Integer.parseInt(isbn.substring(i, i + 1));
				tot += (i % 2 == 0) ? digit * 1 : digit * 3;
			}

			int sumaVerif = 10 - (tot % 10);
			if (sumaVerif == 10) {
				sumaVerif = 0;
			}

			return sumaVerif == Integer.parseInt(isbn.substring(12));
		} catch (NumberFormatException nfe) {
			return false;
		}
	}

	public void setISBN(String iSBN) {
		if (iSBN == null) {
			throw new IllegalArgumentException("ISBN null");
		} else if (iSBN.length() != 17) {
			throw new IllegalArgumentException("Marime ISBN diferita de 17");
		} else if (iSBN.split("-").length != 5) {
			throw new IllegalArgumentException("Format ISBN incorect");
		} else {
			this.ISBN = iSBN;
		}

	}

	public String getTitlu() {
		return titlu;
	}

	public void setTitlu(String _titlu) {
		titlu = _titlu;
	}

	public String getAutori() {
		return autori;
	}

	public void setAutori(String _autori) {
		autori = _autori;
	}

	public String getGen() {
		return gen;
	}

	public void setGen(String _gen) {
		gen = _gen;
	}

	public float getPret() {
		return pret;
	}

	public void setPret(float pret) {
		if (pret < 0) {
			throw new IllegalArgumentException();
		} else {
			this.pret = pret;
		}
	}

	public String toString() {
		String text = "";
		text = this.ISBN + " " + this.titlu + " " + this.autori + " ";
		text += this.getGen() + " ";
		text += this.pret + " ";
		return text;
	}

	public void scriereFisierTextCarteAbstracta(BufferedWriter bw) {
		try {
			bw.write(ISBN + "\t");
			bw.write(titlu + "\t");
			bw.write(autori + "\t");
			bw.write(gen + "\t");
			bw.write(pret + "\t");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void citireFisierTextCarteAbstracta(Scanner scan) {
		this.setISBN(scan.next());
		this.setTitlu(scan.next());
		this.setAutori(scan.next());
		this.setGen(scan.next());
		this.setPret(scan.nextFloat());
	}

	@Override
	public Object clone() {
		Object clone = null;

		try {
			clone = super.clone();

		} catch (CloneNotSupportedException e) {

			e.printStackTrace();
		}

		return clone;
	}

}
