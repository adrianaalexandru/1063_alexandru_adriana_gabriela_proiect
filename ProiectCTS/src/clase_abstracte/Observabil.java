package clase_abstracte;

import java.util.ArrayList;
import java.util.List;

import clase.Client;
import clase.ProcesatorNotificareEmail;
import clase.ProcesatorNotificareTelefon;
import interfete.Observer;
import utils.Utils.STARE_CLIENT_NOTIFICARE;

public abstract class Observabil {
	List<Observer> observers = new ArrayList<>();

	public void adaugaObserver(Observer o) {
		if (observers != null)
			observers.add(o);
	}

	public void stergeObserver(Observer o) {
		if (observers != null)
			observers.remove(o);
	}

	public void notifica(String mesaj) {
		ProcesatorNotificareTelefon procesator = new ProcesatorNotificareTelefon();
		procesator.setSuccessor(new ProcesatorNotificareEmail());

		for (int i = 0; i < observers.size(); i++) {
			procesator.gestioneazaNotificare(observers.get(i), mesaj);
		}
	}
}
