package interfete;

import clase_abstracte.CarteAbstracta;
import clase_abstracte.ComandaAbstracta;
import utils.Utils.STARE_COMANDA;

public interface ICommand {


	public abstract float totalValue()throws Exception ;

	public String favoriteGenre();


	public void addNewCommandElement(CarteAbstracta carteAbstracta, int nr);
	
	public void notifyStateChange(ComandaAbstracta comandaAbstracta);
}
