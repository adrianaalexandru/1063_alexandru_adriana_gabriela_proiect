package interfete;

import clase.Comanda;
import clase_abstracte.ComandaAbstracta;

public interface TipLivrare {

	void livreazaComanda(ComandaAbstracta comanda);
	
}
